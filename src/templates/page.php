<?php get_header();?>
	<main class="container max-in">
		<div class="row">
			<div class="col px-md-5 py-5">
				<?php
					if ( have_posts() ) {
						while (have_posts()) {
							the_post();
							?>
								<?php the_title('<h1 class="mb-5">','</h1>');?>

								<?php the_content(); ?>
						<?php
						}
					} else {
						?>
							<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php
					}
				?>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
