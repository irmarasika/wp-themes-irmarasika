# WP Themes Dev

> WordPress Themes development frontend and backend setup, provides a minimal webpack config for Twitter Bootstrap theming via sass, external javascript modules inclusion and a php router file to serve the project locally with php development server.

Tajid Yakub <tajid.yakub@gmail.com>

## Features

- WordPress themes boilerplate in `src/templates`
- Scss files for Twitter Bootstrap custom theming in `src/assets/scss`
- Fontawesome included in `src/assets/scripts/fa.js`
- Webpack with `sass-loader` and `postcss` with `autoprefixer`configuration
- `watch` mode will automatically process the files on changes

## Installation

Clone this repository to start your WordPress theme development. And install dependencies with `npm install`.

``` bash
$ git clone https://bitbucket.org/wp-themes-dev.git
$ cd wp-themes-dev
$ npm install
```

Use `dev:watch` to start the PHP Development server, learn about another command through the wiki provided.

## Themes Development

Read more on using or extending the setup for WordPress theme development from the Wiki https://bitbucket.org/tajidyakub/wp-themes-dev/wiki/.

## Pull Requests

Pull Requests are encouraged. Drop a message at tajid.yakub@gmail.com
